package fr.norsys.springbootcicd.sii;

import fr.norsys.springbootcicd.bean.AppInfos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AboutService {

	@Autowired
	private Environment env;

	@ResponseBody
	@GetMapping("/about")
	public AppInfos getApplicationInformations() {
		AppInfos appInfos = new AppInfos();

		appInfos.setName(env.getProperty("application.name"));
		appInfos.setVersion(env.getProperty("application.version"));
		appInfos.setAuthor(env.getProperty("application.author"));

		return appInfos;
	}
}
